# HTTP Status Check

Python script to check multiple URL routes against an expected HTTP status code.

## Requirements

* Python3 >=3.7
* pip3

## Installation

Install required modules.

````shell
$ pip3 install -r requirements.txt
````

## Configuration

Create configuration file.

````shell
$ cp dist/config.yaml.dist config.yaml
````

Fit configuration for your needs.

````yaml
domain: https://my-domain.com
expectedHttpStatusCode: 200
routes:
  - /index.html
  - /my/sub/dir/index.html
````

## Usage

Run check script with your configuration.

````shell
$ python3 http_status_check.py --config=/<path>/config.yaml
````

Enable debug output by adding `--debug` flag.

## Parameters

### --config

Set path to configuration YAML file. 

### --debug

Enables debug output.

## Example

**Configuration (`./test/data/config-without-errors.yaml`):**
`````yaml
domain: https://gitlab.com
expectedHttpStatusCode: 200
routes:
  - /
  - /hirnsturm/http-status-check
`````

**Execution:**
````shell
$ python3 http_status_check.py --config=./test/data/config-without-errors.yaml
````

**Output:**
````shell
[INFO] Read configuration from ./test/data/config-without-errors.yaml
[INFO] Check routes against expected status code: 200
[OK] https://gitlab.com/ returns HTTP status 200
[OK] https://gitlab.com/hirnsturm/http-status-check returns HTTP status 200
[OK] Done.
````